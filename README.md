Helper classes that will come in handy if you are working with Anorm and/or Slick

###Dependencies###
* Play 2
* Slick 2
* com.pligor.generic

*Used in the following projects*  
[bman](http://bman.co)  
[Fcarrier](http://facebook.com/FcarrierApp)