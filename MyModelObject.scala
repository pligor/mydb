package com.pligor.mydb

import anorm._
import java.sql.Connection

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyModelObject {
  def tableName: String;

  def getById(id: Long)(implicit connection: Connection): Option[MyModel];

  protected def getById[A](id: Long, parser: RowParser[A])(implicit connection: Connection): Option[A] = {
    SQL("SELECT * FROM " + tableName + " WHERE id = {id}").on(
      "id" -> id
    ).as(parser.singleOpt);
  }
}
