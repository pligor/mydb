package com.pligor.mydb

import play.api.libs.json._

import play.api.Play.current
import java.sql.Connection
import anorm._
import play.api.libs.json.JsObject
import java.io.File
import com.pligor.generic.FileHelper._
import com.pligor.mydb.AnormHelper._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object FixtureLoad {
  val fixturesPath = "test/fixtures"

  val fixtureExtension = "json"

  val fullFixturesPath = current.path.getCanonicalPath + File.separator + fixturesPath

  private def getFiles = {
    new File(fullFixturesPath).list().map {
      filename => filename.substring(0, filename.length - ("." + fixtureExtension).length)
    }
  }

  def clearAllTables(implicit connection: Connection): Seq[String] = {
    val files = getFiles

    files.foreach(tableName => SQL("DELETE FROM " + tableName).executeUpdate())

    files
  }

  def apply()(implicit connection: Connection): Unit = {
    val files = clearAllTables

    files.foreach(FixtureLoad(_, delete = false))
  }

  def apply(tableName: String, delete: Boolean = true)(implicit connection: Connection): Unit = {
    //to do check why TRUNCATE does not work with foreign keys!
    if (delete) {
      SQL("DELETE FROM " + tableName).execute()
    }

    val fileContents = getFileContents(
      fullFixturesPath + File.separator + tableName + "." + fixtureExtension
    )

    val json = Json.parse(fileContents) match {
      case x: JsObject => x
      case _ => throw new ClassCastException
    }

    def jsObjectToMap(jsObj: JsObject) = {
      val columns = jsObj.keys

      //the toList is critical because the set implies that values are unique which is obviously not true
      val jsValues = columns.toList.map(jsObj \)

      val theMap = columns.zip(jsValues).toMap

      val filteredMap = theMap.filter {
        case (_, JsNull) => false
        case _ => true
      }

      filteredMap.map {
        case (z, y: JsString) => z -> y.value
        case (i, j: JsBoolean) => i -> j.value
        case (k, l) => k -> l.toString()
      }
    }

    val keys = json.keys
    for (key <- keys) {
      val curRow = json \ key match {
        case x: JsObject => x
        case _ => throw new ClassCastException
      }

      val curMap = jsObjectToMap(curRow)
      //println(curMap.mkString("\n"));
      //println(curMap("language").length);
      insertInto(tableName, curMap)
    }
  }
}
