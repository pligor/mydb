package com.pligor.mydb

import play.api.Application
import play.api.db.DB
import scala.slick.driver.MySQLDriver.simple._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SlickHelper {
  def onDBsession[T](onSession: Session => T)(implicit app: Application): T = {
    Database.forDataSource(DB.getDataSource()).withSession(onSession)
  }
}
