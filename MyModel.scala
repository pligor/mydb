package com.pligor.mydb

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
abstract class MyModel(val id: Option[Long]) {
  def isOld = id.isDefined;
  def isNew = !isOld;

  override def equals(that: Any): Boolean = {
    if (this.getClass != that.getClass) {
      throw new NotSameClassException("you are trying to compare models which do not belong to the same class");
    }

    val thatModel = that match {
      case x: MyModel => x;
      case _ => throw new ClassCastException("because of the previous check we shall not reach this case");
    }

    if (!this.id.isDefined || !thatModel.id.isDefined) {
      throw new NoDefinedIdsException("at least one of the models you are trying to compare for equality has no defined ids");
    }

    this.id.get == thatModel.id.get;
  }

  private class NoDefinedIdsException(str: String) extends Exception(str);
  private class NotSameClassException(str: String) extends Exception(str);
}
