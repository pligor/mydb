package com.pligor.mydb

import anorm._
import anorm.SqlParser._
import java.sql.Connection
import play.api.db.DB
import play.api.Application
import scala.language.implicitConversions

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AnormHelper {
  private val defaultIdColumn = "id"

  val MySQLTimestampFormat = "yyyy-MM-dd HH:mm:ss"

  def withNoForeignKeys[A](block: (Connection) => A)(implicit application: Application): A = {
    DB.withConnection {
      implicit connection =>
        noForeignKeys(block)
    }
  }

  def withNoForeignKeys[A](name: String)(block: (Connection) => A)
                          (implicit application: Application): A = {
    DB.withConnection(name) {
      implicit connection =>
        noForeignKeys(block)
    }
  }

  private def noForeignKeys[A](block: (Connection) => A)(implicit connection: Connection): A = {
    disableAndStoreStateOfForeignKeys
    val result = block(connection)
    restoreStateOfForeignKeys
    result
  }

  private def disableAndStoreStateOfForeignKeys(implicit connection: Connection): Unit = {
    SQL("SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;").execute()
    SQL("SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;").execute()
    SQL("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';").execute()
  }

  private def restoreStateOfForeignKeys(implicit connection: Connection): Unit = {
    SQL("SET SQL_MODE=@OLD_SQL_MODE;").execute()
    SQL("SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;").execute()
    SQL("SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;").execute()
  }

  class MySimpleSqlWrapper[T](wrapped: SimpleSql[T])(implicit connection: Connection) {
    def anormResultToBoolean = {
      integerToBoolean(
        wrapped.as(scalar[Long].single)
      )
    }
  }

  implicit def toMySimpleSqlWrapper[T](simpleSql: SimpleSql[T])(implicit connection: Connection) = {
    new MySimpleSqlWrapper[T](simpleSql)
  }

  def integerToBoolean(integer: Long): Boolean = integer match {
    case 0 => false
    case 1 => true
    case _ => throw new IdenticalRowsException("this is not possible")
  }

  def updateSingleColumnById[T](tableName: String,
                                columnName: String,
                                columnValue: T,
                                id: Long,
                                idColumn: String = defaultIdColumn)
                               (implicit connection: Connection): Boolean = {
    integerToBoolean(
      SQL("UPDATE " + tableName + " SET " + columnName + " = {columnValue} WHERE " + idColumn + " = {id}").on(
        "columnValue" -> columnValue,
        "id" -> id
      ).executeUpdate()
    )
  }

  /**
   * @param tableName no problem to alter a table which does not have an auto increment column, nothing happens
   */
  def resetAutoIncrement(tableName: String, based: Int = 1)(implicit connection: Connection): Unit = {
    SQL("ALTER TABLE " + tableName + " AUTO_INCREMENT = {based}").on("based" -> based).execute()(connection)
  }

  def tableRowExists(tableName: String, id: Long, idColumn: String = defaultIdColumn)
                    (implicit connection: Connection): Boolean = {
    tableRowExists(tableName, Map(idColumn -> id))
  }

  def tableRowExists(tableName: String, compoundId: Map[String, Long])(implicit connection: Connection): Boolean = {
    workWithCompoundId(compoundId) {
      (whereClause, params) =>
        integerToBoolean(
          SQL("SELECT COUNT(*) FROM " + tableName + " WHERE " + whereClause).
            on(params: _ *).as(scalar[Long].single)
        )
    }
  }

  private def workWithCompoundId[T](compoundId: Map[String, Long])
                                   (op: (String, Seq[(Any, ParameterValue[_])]) => T): T = {
    val whereClause = compoundId.map(curId => curId._1 + " = {" + curId._1 + "}").mkString(" AND ");
    val params: Seq[(Any, ParameterValue[_])] = compoundId.map(
      curId =>
        curId._1 -> toParameterValue(curId._2)
    ).toSeq

    op(whereClause, params)
  }

  /**
   * works only on tables with a single (not compound) primary key
   */
  def deleteById(tableName: String, id: Long, columnName: String = defaultIdColumn)(implicit connection: Connection): Boolean = {
    integerToBoolean(
      SQL("DELETE FROM " + tableName + " WHERE " + columnName + " = {id}").on("id" -> id).
        executeUpdate()
    )
  }

  /**
   * @throws MySQLIntegrityConstraintViolationException when duplicate entry because of primary key
   * @return Option[T] where T is the type of the auto increment primary key. If PK is not autoincrement, then None is returned
   */
  def insertInto(tableName: String, row: Map[String, _])(implicit connection: Connection) = {
    //convert from Iterable to Sequence because otherwise the order of the elements is not consistent
    val keys = row.keys.toSeq

    val columns = keys.mkString(",")

    val placeholders = keys.map("{" + _ + "}").mkString(",")

    val params: Seq[(Any, ParameterValue[_])] = row.map(
      (x: (String, _)) => x._1 -> toParameterValue(x._2)
    ).toSeq

    val sqlString = "INSERT INTO " + tableName + "(" + columns + ") VALUES(" + placeholders + ")"

    SQL(sqlString).on(params: _*).executeInsert()
  }
}
