package com.pligor.mydb

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class IdenticalRowsException(str: String) extends Exception(str);
